package fenYeShiXian;

import com.ruoyi.common.core.page.TableDataInfo;
import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

import static com.ruoyi.common.utils.PageUtils.startPage;

public class FYSX {


//    @PostMapping("/list")
//    @ResponseBody
//    public TableDataInfo list (SecurityProperties.User user){
//        startPage();
//        List<SecurityProperties.User> List = userService.selectUserList(user);
//        return getDataTable(list);
//    }


//    示例错误代码1      莫名其妙的分页



//    startPage();
//    List<User> list;
//    if(user != null){
//        list = userService.selectUserList(user);
//    } else {
//        list = new ArrayList<User>();
//    }
//    Post post = postService.selectPostById(1L);
//    return getDataTable(list);

//    user 存在 null 的 情况 导致 pageHelper 生产了 分页 参数

//    解决方式

//    List<User> list;
//    if(user != null){
//        startPage();
//        list = userService.selectUserList(user);
//    } else {
//        list = new ArrayList<User>();
//    }
//    Post post = postService.selectPostById(1L);
//    return getDataTable(list);





//    示例错误 2  添加了startPage方法。也没有正常分页。例如下面这段代码

//    startPage();
//    Post post = postService.selectPostById(1L);
//    List<User> list = userService.selectUserList(user);
//    return getDataTable(list);

//    原因分析：只对该语句以后的第一个查询（Select）语句得到的数据进行分页。
//    上面这个代码，应该写成下面这个样子才能正常分页。

//    Post post = postService.selectPostById(1L);
//    startPage();
//    List<User> list = userService.selectUserList(user);
//    return getDataTable(list);

}

package com.ruoyi.system.service.impl;

import com.ruoyi.system.domain.UufsXsxxLjt;

import java.util.List;

public interface UufsXsxxLjtService {

    List<UufsXsxxLjt> selectUufsXsxxLjtList(UufsXsxxLjt uufsXsxxLjt);

    int insertUufsXsxxLjt(UufsXsxxLjt uufsXsxxLjt);

    UufsXsxxLjt seleceUufsXsxxLjtById(Long xh);

    int updateUufsXsxxLjt(UufsXsxxLjt uufsXsxxLjt);

    int deleteUufsXsxxLjtById(String ids);
}

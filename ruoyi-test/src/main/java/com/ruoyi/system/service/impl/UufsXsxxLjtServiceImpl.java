package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.text.Convert;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.system.domain.UufsXsxxLjt;
import com.ruoyi.system.mapper.UufsXsxxLjtMapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class UufsXsxxLjtServiceImpl implements UufsXsxxLjtService {

    @Autowired
    private UufsXsxxLjtMapper uufsXsxxLjtMapper;

    /**
     * 查询学生管理列表
     * @param uufsXsxxLjt
     * @return
     */
    @Override
    public List<UufsXsxxLjt> selectUufsXsxxLjtList(UufsXsxxLjt uufsXsxxLjt) {
        return uufsXsxxLjtMapper.selectUufsXsxxLjtList(uufsXsxxLjt);
    }

    @Override
    public int insertUufsXsxxLjt(UufsXsxxLjt uufsXsxxLjt) {
        uufsXsxxLjt.setCreateTime(DateUtils.getNowDate());
        return uufsXsxxLjtMapper.insertUufsXsxxLjt(uufsXsxxLjt);
    }

    @Override
    public UufsXsxxLjt seleceUufsXsxxLjtById(Long xh) {
        return uufsXsxxLjtMapper.selectUufsXsxxLjtById(xh);
    }

    @Override
    public int updateUufsXsxxLjt(UufsXsxxLjt uufsXsxxLjt) {
        uufsXsxxLjt.setUpdateTime(DateUtils.getNowDate());
        return uufsXsxxLjtMapper.updateUufsXsxxLjt(uufsXsxxLjt);
    }

    @Override
    public int deleteUufsXsxxLjtById(String ids) {
       return uufsXsxxLjtMapper.deleteUufsXsxxLjtByIds(Convert.toStrArray(ids));
    }
}

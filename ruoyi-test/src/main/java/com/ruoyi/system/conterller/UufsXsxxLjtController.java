package com.ruoyi.system.conterller;


import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.system.domain.UufsXsxxLjt;
import com.ruoyi.system.service.impl.UufsXsxxLjtService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/system/Ljt")
public class UufsXsxxLjtController extends BaseController {


    private String prefix = "system/ljt";

    @Autowired
    private UufsXsxxLjtService uufsXsxxLjtService;


    @RequiresPermissions("system:ljt:view")
    // 需要权限
    @GetMapping()
    public String ljt(){
        return prefix + "/ljt";
    }


    /**
     * 查询学生管理列表
     * @param uufsXsxxLjt
     * @return
     */
    @RequiresPermissions("system:ljt:list")
    @PostMapping("list")
    @ResponseBody
    public TableDataInfo list(UufsXsxxLjt uufsXsxxLjt){
        startPage();
        List<UufsXsxxLjt> list = uufsXsxxLjtService.selectUufsXsxxLjtList(uufsXsxxLjt);
        return getDataTable(list);
    }



    /**
     * 导出学生管理表
     * @param uufsXsxxLjt
     * @return
     */
    @RequiresPermissions("system:ljt:export")
    @Log(title = "学生管理",businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(UufsXsxxLjt uufsXsxxLjt){
        List<UufsXsxxLjt> list = uufsXsxxLjtService.selectUufsXsxxLjtList(uufsXsxxLjt);
        ExcelUtil<UufsXsxxLjt> util = new ExcelUtil<>(UufsXsxxLjt.class);
        return util.exportExcel(list,"学生管理数据");
    }

    /**
     * 新增学生管理
     * @return
     */
    @GetMapping("/add")
    public String add(){
        return prefix + "/add";
    }

    /**
     * 新增保存学生管理
     * @param uufsXsxxLjt
     * @return
     */

    @RequiresPermissions("system:ljt:add")
    @Log(title = "学生管理" ,businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave (UufsXsxxLjt uufsXsxxLjt){
        return toAjax(uufsXsxxLjtService.insertUufsXsxxLjt(uufsXsxxLjt));
    }

    /**
     * 修改学生管理
     * @param xh
     * @param mmap
     * @return
     */
    @GetMapping("/edit/{xh}")
    public String edit(@PathVariable("xh") Long xh, ModelMap mmap){
        UufsXsxxLjt uufsXsxxLjt = uufsXsxxLjtService.seleceUufsXsxxLjtById(xh);
        mmap.put("uufsXsxxLjt",uufsXsxxLjt);
        return prefix + "/edit";
    }

    /**
     * 修改保存学生管理
     * @param uufsXsxxLjt
     * @return
     */
    @RequiresPermissions("system:ljt:edit")
    @Log(title = "学生管理",businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(UufsXsxxLjt uufsXsxxLjt){
        return toAjax(uufsXsxxLjtService.updateUufsXsxxLjt(uufsXsxxLjt));
    }


    public AjaxResult remove(String ids){
        return toAjax(uufsXsxxLjtService.deleteUufsXsxxLjtById(ids));
    }











}

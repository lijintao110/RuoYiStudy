package com.ruoyi.system.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

public class UufsXsxxLjt extends BaseEntity {

    private static final long serialVersionUID = 1L;

    private Long xh;

    @Excel(name = "姓名")
    private String xm;

    @Excel(name = "性别")
    private String xb;

    @Excel(name = "出生日期")
    private String csrq;

    @Excel(name = "院系名称")
    private String yxmc;

    @Excel(name = "院系代码")
    private String yxdm;


    public Long getXh() {
        return xh;
    }

    public void setXh(Long xh) {
        this.xh = xh;
    }

    public String getXm() {
        return xm;
    }

    public void setXm(String xm) {
        this.xm = xm;
    }

    public String getXb() {
        return xb;
    }

    public void setXb(String xb) {
        this.xb = xb;
    }

    public String getCsrq() {
        return csrq;
    }

    public void setCsrq(String csrq) {
        this.csrq = csrq;
    }

    public String getYxmc() {
        return yxmc;
    }

    public void setYxmc(String yxmc) {
        this.yxmc = yxmc;
    }

    public String getYxdm() {
        return yxdm;
    }

    public void setYxdm(String yxdm) {
        this.yxdm = yxdm;
    }



    @Override
    public String toString() {
        return "UufsXsxxLjt{" +
                "xh=" + xh +
                ", xm='" + xm + '\'' +
                ", xb='" + xb + '\'' +
                ", csrq='" + csrq + '\'' +
                ", yxmc='" + yxmc + '\'' +
                ", yxdm='" + yxdm + '\'' +
                '}';
    }
}

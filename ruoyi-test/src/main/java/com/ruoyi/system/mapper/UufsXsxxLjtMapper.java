package com.ruoyi.system.mapper;

import com.ruoyi.system.domain.UufsXsxxLjt;

import java.util.List;

public interface UufsXsxxLjtMapper {

    List<UufsXsxxLjt> selectUufsXsxxLjtList(UufsXsxxLjt uufsXsxxLjt);

    int insertUufsXsxxLjt(UufsXsxxLjt uufsXsxxLjt);

    UufsXsxxLjt selectUufsXsxxLjtById(Long xh);

    int updateUufsXsxxLjt(UufsXsxxLjt uufsXsxxLjt);

    int deleteUufsXsxxLjtByIds(String[] toStrArray);
}

